<?php

class Connection
{
    protected $host = ${DB_HOST};
    protected $database = ${DB_DATABASE};
    protected $username = ${DB_USERNAME};
    protected $password = ${DB_PASSWORD};

    public function __construct()
    {
        $this->connect();
    }
 
    private function connection()
    {
        $pdo = null;
        try {
            $pdo = new PDO("mysql:host=' . $this->host . ';dbname=' . $this->database . '", $this->username, $this->password);
        } catch (PDOException $e) {
            print $e->getMessage();
            die();
        }
        var_dump($this->password);
    }
}